-- https://www.hammerspoon.org/docs/hs.window.html#animationDuration
hs.window.animationDuration = 0

-- https://www.hammerspoon.org/go/#fancyreload
-- https://www.hammerspoon.org/Spoons/ReloadConfiguration.html
hs.loadSpoon("ReloadConfiguration")
spoon.ReloadConfiguration:start()

-- https://github.com/miromannino/miro-windows-manager
hs.loadSpoon("MiroWindowsManager")
spoon.MiroWindowsManager.sizes = { 6/5, 4/3, 3/2, 2/1, 3/1, 4/1, 6/1 }
spoon.MiroWindowsManager.fullScreenSizes = {1, 6/5, 4/3, 2}
spoon.MiroWindowsManager:bindHotkeys({
  up = {{}, 'k'},
  down = {{}, 'j'},
  right = {{}, 'l'},
  left = {{}, 'h'},
  fullscreen = {{}, 'w'},
  middle = {{}, 'm'},
  moveWindowOneSpaceLeft = {{}, '['},
  moveWindowOneSpaceRight = {{}, ']'},
  moveWindowOneSpaceLeftSticky = {{"alt"}, '['},
  moveWindowOneSpaceRightSticky = {{"alt"}, ']'}
})

local function toggleApp(name)
  local f = function ()
    local app = hs.application.find(name)
    if not app or app:isHidden() then
      hs.application.launchOrFocus(name)
    elseif hs.application.frontmostApplication() ~= app then
      app:activate()
    else
      app:hide()
    end
  end
  return f
end

hs.hotkey.bind({"cmd", "ctrl", "shift"}, "f", toggleApp("Finder"))
hs.hotkey.bind({"cmd", "ctrl", "shift"}, "d", toggleApp("Dictionary"))
hs.hotkey.bind({"cmd", "ctrl", "shift"}, "t", toggleApp("iTerm"))
hs.hotkey.bind({"cmd", "ctrl", "shift"}, "x", toggleApp("Firefox"))

-- https://github.com/Hammerspoon/hammerspoon/issues/1499
local alertLayout = hs.distributednotifications.new(
  function(name, object, userInfo)
    local currentLayout = hs.keycodes.currentLayout()
    hs.alert.show(currentLayout)
  end,
  -- or 'AppleSelectedInputSourcesChangedNotification'
  "com.apple.Carbon.TISNotifySelectedKeyboardInputSourceChanged"
)
alertLayout:start()

-- Window Hints
-- hs.hints.style = 'vimperator'
-- hs.hotkey.bind({'shift', 'ctrl', 'alt', 'cmd'}, 'm', hs.hints.windowHints)

-- require 'ctrlDoublePress'
-- require 'caffeine'
